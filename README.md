# README #

Fcarena Interview

### What is this repository for? ###

* Interview Exam
* Version 1.0


### How do I get set up? ###

* Install Mysql
  * Predefined user is in settings.py, you can change it
* Install Memcached
  * Predefined cache conf in settings.py, you can change it
* run requirement.txt
* run django migrations
* in django admin you can configure the values of the app
* to get the values you can curl with this example
curl --location --request GET 'localhost:port/<KeyToLook>'
  