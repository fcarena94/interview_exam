from django.apps import AppConfig


class AppRedirectConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_redirect'

    def ready(self):
        import app_redirect.signals
