from itertools import chain
from django.core.cache import cache

def to_dict(instance):
    """Function to parse model to dict"""
    """I dont like model_to_dict embeeded with django models"""
    model = instance._meta
    data = {}
    for f in chain(model.concrete_fields, model.private_fields):
        data[f.name] = f.value_from_object(instance)
    for f in model.many_to_many:
        data[f.name] = [i.id for i in f.value_from_object(instance)]
    return data


def get_key_from_cache(key):
    """Retrieves data from cache"""
    response = cache.get(key)
    if response:
        return {'Key': response['key'], 'Url': response['url']}
    else:
        return {'Error': 'No entry in cache for %s' % key}


