# Generated because i forgot to add unique to key field

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_redirect', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='redirect',
            name='key',
            field=models.CharField(max_length=50, unique=True),
        ),
    ]
