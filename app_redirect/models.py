from logging import exception

from django.db import models
from django.core.cache import cache
from app_redirect.helpers import to_dict


class Redirect(models.Model):
    """Data model for redirect table"""
    """
    Key = String
    Url = String
    Active = Bool
    Updated_at = Datetime
    Created_at = Datetime
    """
    key = models.CharField(max_length=50, null=False, unique=True)
    url = models.CharField(max_length=200, null=False)
    active = models.BooleanField(null=False, default=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    @classmethod
    def find_redirect_by_key(cls, key):
        """Returns model by key"""
        redirect = cls.objects.get(key=key)
        return redirect

    @classmethod
    def set_cache_for_active(cls):
        """Set cache for al active models in db"""
        all_objects = cls.objects.all().filter(active=True)
        cache._cache.flush_all()
        for object in all_objects:
            cache.set(object.key, to_dict(object))

    @classmethod
    def delete_cache_key(cls, key):
        """Deletes cache entry when model is deleted and raise error if occurs"""
        try:
            cache.delete(key)
        except exception:
            raise exception('Could not delete cached key')

    @classmethod
    def save_in_cache(cls, key):
        """Saves entry in cache when model is created in db"""
        redirect_object = cls.objects.get(key=key)
        cache.set(redirect_object.key, to_dict(redirect_object))
