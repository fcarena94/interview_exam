from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver
from app_redirect.models import Redirect


@receiver(pre_save, sender=Redirect)
def pre_save_redirect(sender, instance, **kwargs):
    """Function to notice updates and modify cache depending on status"""
    if not instance._state.adding and instance.active:
        Redirect.set_cache_for_active()
    if not instance._state.adding and not instance.active:
        Redirect.delete_cache_key(instance.key)


@receiver(post_save, sender=Redirect)
def post_save_redirect(sender, instance, **kwargs):
    """Creates cache entry when model is created and active equals True"""
    if instance.active:
        Redirect.save_in_cache(instance.key)


@receiver(post_delete, sender=Redirect)
def post_delete_redirect(sender, instance, **kwargs):
    """Deletes cache entry when model is deleted in db"""
    Redirect.delete_cache_key(instance.key)
