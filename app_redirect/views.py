from django.http import JsonResponse
from django.views import View
from app_redirect.helpers import get_key_from_cache


class ApiResponses(View):
    def get(self, request, key):
        """Simple function to retrieve objects from cache"""
        response = get_key_from_cache(key)
        return JsonResponse(response)
