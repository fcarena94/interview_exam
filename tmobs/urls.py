from django.contrib import admin
from django.urls import path, include
from app_redirect.views import ApiResponses

urlpatterns = [
    path('admin/', admin.site.urls),
    path('<key>', ApiResponses.as_view()),
]
